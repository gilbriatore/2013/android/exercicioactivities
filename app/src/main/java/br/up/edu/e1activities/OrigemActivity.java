package br.up.edu.e1activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class OrigemActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_origem);
    }

    public void ir(View v){

        Intent intent = new Intent(this, DestinoActivity.class);

        String texto = "Olá seja bem-vindo!";
        intent.putExtra("id999", texto);

        startActivity(intent);

    }
}