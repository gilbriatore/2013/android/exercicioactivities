package br.up.edu.e1activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

public class DestinoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_destino);

        Intent intent = getIntent();
        String texto = intent.getStringExtra("id999");

        EditText txtMensagem = (EditText) findViewById(R.id.txtMensagem);
        txtMensagem.setText(texto);

    }
}